<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>WordPress Comments</title>

	<link rel="stylesheet" href="dist/css/screen.min.css">
	<style>
	.pr-0 {
		padding-right: 0;
	}
	.pl-0 {
		padding-left: 0;
	}
	.button.small {
		font-size: .9em;
		padding: 2px 6px;
	}
	.error > *:first-child,
	.notice > *:first-child {
		margin-top: 0;
	}
	.error > *:last-child,
	.notice > *:last-child {
		margin-bottom: 0;
	}
	</style>
</head>
<body>

<!--[if lt IE 7]>
	<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div id="headerContainer" class="grid-parent grid-100 tablet-grid-100 mobile-grid-100">
	
	<div id="site-header" class="grid-parent f960 tablet-grid-100 mobile-grid-100">
        <div class="grid-66 tablet-grid-66 mobile-grid-100">
            <a href="http://www.highpoint.edu/" rel="home" class="site-title" title="High Point University">
                <img src="dist/images/high-point-university-logo-purple.png" alt="High Point University">
            </a>
        </div>
        <div class="grid-33 tablet-grid-33 hide-on-mobile">
            <h1 class="alignright"><a href="<?php echo wpc_config::$url; ?>" title="My Password">WordPress Comments</a></h1>
        </div>
	</div>
	
	<!-- End #headerContainer -->
</div>

<div id="section1Container" class="sectionGroup grid-parent grid-100 table-grid-100 mobile-grid-100">
	<div class="shadowCover"><!-- This div is for hiding the drop shadow from the current menu item. --></div>
	<div class="grid-parent f960 tablet-grid-100 mobile-grid-100">

# WPMU Comments

Counts the comments on all blogs of a WordPress Multisite instance.

This is not a WordPress plugin.

## Usage

1. Place files in your document root in a folder name 'wpmu-comments'.
2. Copy config-sample.php to config.php.
3. Set location of wp-load.php.
4. Set URL.
/*
 * The Gulp file for the highpoint theme.
 */

// Load plugins
var gulp = require('gulp'),
	plugins = require('gulp-load-plugins')({ camelize: true }),
	del = require('del'),
	lr = require('tiny-lr'),
	server = lr();


// Custom Functions - - - - - - - - - - - - - - - - - - - - -


/*
 * A function used to wrap messages.
 */
var wrapMsg = function( head, msg ) {
	// This variable will be used to store the 
	// length of the longest line. Check the 
	// header first.
	var l = head.length;

	// Split the message lines
	var lines = msg.split("\n");

	// Lets find the longest line
	lines.forEach(function(line) {
		if ( line.length > l ) {
			l = line.length;
		}
	})

	// Some empty space
	output = "\n\n";

	// A line of dashes longer then the longest line
	output = output + Array(l+4).join('—') + "\n";

	// The header for this message
	output = output + "⎮ " + head + "\n";

	// A line of alternating spaces/dashes)
	output = output + "⎮" + Array( Math.ceil((l+2)) ).join('—') + "—\n";

	// Each line of the message
	lines.forEach(function(line) {
		output = output + "⎮ " + line + "\n";
	})

	// A closing line of dashes
	output = output + "⎮" + Array(l+2).join('—');

	// return with some empty space after.
	return output + "—\n\n";
}


// Our tasks - - - - - - - - - - - - - - - - - - - - - - - - -


/*
 * A task to clean the distribution folder.
 */
 gulp.task('clean', function() {
 	return del('dist/**');
 });


/*
 * The styles task.
 */
gulp.task('styles', function( cb ) {
	return gulp.src(['assets/sass/*.scss'])
		.pipe(plugins.compass({
			project: __dirname,
			css: "dist/css",
			sass: "assets/sass",
			image: "assets/images",
			javascript: false,
			font: "assets/fonts",
			style: 'expanded',
			relative: true,
			comments: false,
			require: ['compass-normalize']
		}))
		.on('error', function(err) {
			console.log( wrapMsg(err.name, err.message) );
		})
		.pipe(plugins.livereload(server))
		.pipe(gulp.dest('dist/css'))
		.pipe(plugins.notify({ message: 'Updated Style: <%= file.relative %>.' }));
});


/*
 * The styles deploy task.
 */
gulp.task('styles-deploy', ['styles'], function() {

	// Clean the old minified versions
	del('dist/css/*.min.css', {force: true});

	return gulp.src(['dist/css/*.css','!dist/css/*.min.css'])
		.pipe(plugins.minifyCss({ keepSpecialComments: 1 }))
		.pipe(plugins.rename({ suffix: '.min' }))
		.pipe(gulp.dest('dist/css'));
});


/*
 * Vendor Plugin Scripts task.
 */
gulp.task('ie-scripts', function() {
	return gulp.src(['assets/js/IE/*.js'])
		.pipe(plugins.concat('ie.js'))
		.pipe(plugins.livereload(server))
		.pipe(gulp.dest('dist/js'))
		.pipe(plugins.notify({ message: 'Plugin Updated: <%= file.relative %>.' }));
});

/*
 * Vendor Plugin Scripts task.
 */
gulp.task('plugins', function() {
	return gulp.src(['assets/js/plugins/*.js', 'assets/js/vendor/*.js'])
		.pipe(plugins.concat('plugins.js'))
		.pipe(plugins.livereload(server))
		.pipe(gulp.dest('dist/js'))
		.pipe(plugins.notify({ message: 'Plugin Updated: <%= file.relative %>.' }));
});


/*
 * The scripts task for our custom scripts.
 */
gulp.task('scripts', function() {
	return gulp.src(['assets/js/*.js'])
		.pipe(plugins.jshint('.jshintrc'))
		.pipe(plugins.jshint.reporter('default'))
		.pipe(plugins.concat('main.js'))
		.pipe(plugins.livereload(server))
		.pipe(gulp.dest('dist/js'))
		.pipe(plugins.notify({ message: 'Updated Script: <%= file.relative %>.' }));
});


/*
 * The scripts task for our custom scripts.
 */
gulp.task('scripts-deploy', ['ie-scripts', 'plugins', 'scripts'], function() {

	// Clean the old minified versions
	del('dist/js/*.min.js');

	return gulp.src(['dist/js/*.js'])
		.pipe(plugins.rename({ suffix: '.min' }))
		.pipe(plugins.uglify())
		.pipe(gulp.dest('dist/js'));
});


/*
 * Our images task.
 */
gulp.task('images', ['styles'], function() {
	return gulp.src([
			'assets/images/**/*', '!assets/images/alert-icons/*'])
		.pipe(plugins.cached('images', { optimizeMemory: true }))
		.pipe(plugins.livereload(server))
		.pipe(gulp.dest('dist/images'))
		.pipe(plugins.notify({ message: 'Updated Image: <%= file.relative %>.' }));
});


/*
 * Our images deployment task.
 */
gulp.task('images-deploy', ['styles'], function() {

	// Clean the old images
	del('dist/images/**/*');

	return gulp.src([
			'assets/images/**/*'])
		.pipe(plugins.cached('images', { optimizeMemory: true }))
		.pipe(plugins.imagemin({ optimizationLevel: 7, progressive: true, interlaced: true }))
		.pipe(gulp.dest('dist/images'));
});


/*
 * Our fonts task.
 */
gulp.task('fonts', function() {
	return gulp.src(['assets/fonts/*.ttf', 'assets/fonts/*.eot', 'assets/fonts/*.svg', 'assets/fonts/*.woff'])
		.pipe(gulp.dest('dist/fonts'));
});


/*
 * The watch task.
 */
gulp.task('watch', function() {

	// Listen on port 35729
	server.listen(35729, function (err) {
		
		if (err) {
			return console.log(err)
		};

		// Watch .scss files
		gulp.watch('assets/sass/*.scss', ['styles']);

		// Watch .js files
		gulp.watch('assets/js/**/*.js', ['plugins', 'scripts']);

	});

});


/*
 * An images watch task.
 */
gulp.task('watch-images', ['images'], function() {

	// Listen on port 35729
	server.listen(35729, function (err) {
		
		if (err) {
			return console.log(err)
		};

		// Watch .scss files
		gulp.watch('assets/images/**/*', ['images']);

	});

});



// Groups of tasks - - - - - - - - - - - - - - - - - - - - - -


// Distribution task
gulp.task('deploy', ['styles-deploy', 'scripts-deploy', 'images-deploy', 'fonts'])

// Development task
gulp.task('dev', ['styles', 'ie-scripts', 'plugins', 'scripts', 'watch']);

// Default task
gulp.task('default', ['dev']);


// The End - - - - - - - - - - - - - - - - - - - - - - - - - -

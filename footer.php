	</div>

	<!-- End #section1Container -->
</div>

<div id="footerContainer" class="grid-parent grid-100 tablet-grid-100 mobile-grid-100">
	<div class="footer grid-parent f960 tablet-grid-100 mobile-grid-100">

		<div class="grid-80 tablet-grid-80 mobile-grid-100">
			&nbsp;
		</div>

		<div class="grid-20 tablet-grid-20 mobile-grid-100">
			<img src="dist/images/high-point-university-logo-stacked-purple.png" alt="High Point University" class="grid-100 aligncenter" style="max-width: 172px;">
		</div>

		<!-- End .grid-parent f960 .tablet-grid-100 .mobile-grid-100 -->
	</div>

	<!-- End #footerContainer -->
</div>

<!-- Get jQuery 2.0 from Google. -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<?php do_action('wpmu-comments-footer') ?>

</body>
</html>

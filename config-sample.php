<?php 
/**
 * Config
 * 
 * @author Lucas Bonner
 * @package WP Comments
 * @since WP Comments 0.1
 */

/**
* Config
* 
* since
*/
class wpc_config
{


	/* - - Start editing - - */


	/**
	 * Relative location of wp-load.php.
	 * 
	 * This location is relative to this directory.
	 * 
	 * @since 0.1
	 */
	static $wp_load = '../wp-load.php';


	/**
	 * Base URL.
	 * 
	 * This can be relative from the domain root.
	 * 
	 * @since 0.1
	 */
	static $url = '/wpmu-comments/';


	/* - - Stop editing - - */


	function __construct()
	{
		self::$wp_load = dirname(__FILE__) . self::$wp_load;
	}
}

new wpc_config();

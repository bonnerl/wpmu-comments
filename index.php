<?php 
/**
 * Manage comments on all WordPress multisite blogs.
 * 
 * @author Lucas Bonner
 * @package WP Comments
 * @since WP Comments 0.1
 * @version 0.1
 */

// Status results messages. 
$results = array();
$error = false;

// Get the config.
require_once dirname(__FILE__) .'/config.php';

// Load WordPress functionality.
require_once wpc_config::$wp_load;

// Get functions
require_once dirname(__FILE__) .'/functions.php';

/**
 * Add auto progress to next blog script to footer.
 * 
 * @since WP Comments 0.1
 */
function next_blog_script()
{
	?>
	<script type="text/javascript">
		function nextpage() {
			window.location = $('#delete-comments-next-blog').attr('href');
		}
		setTimeout( "nextpage()", 500 );
	</script>
	<?php
}


// If the user isn't a super admin.
if ( false == is_super_admin() ) {
	require_once 'unauthorized.php';
	return false;
} else {

	if ( isset($_GET['action']) ) {
		switch($_GET['action']) {
			case 'delete_comments':

				if ( isset($_GET['b_id']) && ctype_digit($_GET['b_id']) ) {

					// If we're deleting comments from all blogs then b_id is the array index id.
					if ( isset($_GET['all_blogs']) && '1' == $_GET['all_blogs'] ) {
						$b_id = hpu_get_blogs();
						$b_id = $b_id[$_GET['b_id']]['blog_id'];

						if ( $_GET['b_id'] < (count(hpu_get_blogs())-1) )
							$next = ((int) $_GET['b_id']) + 1;

					} else
						$b_id = $_GET['b_id']; // The actual blog ID.

					switch_to_blog($b_id);

					$results[] = array(get_bloginfo('name'), 'h5');

					if ( isset($_GET['confirm']) && '1' == $_GET['confirm'] ) {

						if ( ! hpu_delete_all_comments($b_id, $results) ) 
							$error = true;

						if ( isset($next) ) {
							$results[] = sprintf('Blog %d of %d.', $_GET['b_id']+1, count(hpu_get_blogs()));
							$results[] = sprintf('<a id="delete-comments-next-blog" href="?action=delete_comments&amp;b_id=%d&amp;confirm=1%s&amp;all_blogs=1" class="button">Next Site</a>', $next, $all_blogs);

							// If there was no error auto progress to the next blog.
							if ( ! $error )
								add_action('wpmu-comments-footer', 'next_blog_script');
						}



					} else {
						$results[] = 'Are you sure you want to delete comments from all blogs?';
						$results[] = '<strong>This cannot be undone!</strong>';
						$results[] = sprintf('<a href="?action=delete_comments&amp;b_id=%d&amp;confirm=1" class="button">Delete Comments</a>', $b_id);
					}

					restore_current_blog();

				} else {

					$results[] = 'An invalid blog ID was given to delete comments.';
					$error = true;

				}
			
				break;

			case 'delete_comments_all_blogs':

				$results[] = 'Are you sure you want to delete all comments?';
				$results[] = '<strong>This cannot be undone!</strong>';
				$results[] = sprintf('<a href="?action=delete_comments&amp;b_id=1&amp;all_blogs=1&amp;confirm=1" class="button">Delete Comments From All Blogs</a>', $b_id);
			
				break;

			default:
				$results[] = 'Invalid Action';
				$error = true;
				break;
		}
	}

}

include_once 'header.php';
?>

<p><strong>Blog Count</strong>: <?php echo count(hpu_get_blogs()); ?></p>

<div class="grid-66 pl-0">
	<h2 class="mt-0">Comment Count</h2>
	<table class="grid-parent grid-100">
		<thead>
			<tr>
				<th>ID</th>
				<th>Site Name</th>
				<th>Comments</th>
				<td></td>
			</tr>
		</thead>
		<tbody>
			<?php 
			$total = 0;
			$most = 0;
			foreach ( hpu_get_blogs() as $details ) :

				switch_to_blog( $details['blog_id'] );

				$count = hpu_comment_count();

				if ( $most < $count )
					$most = $count;

				$total += $count;
				?>
				<tr>
					<td><?php echo $details['blog_id']; ?></td>
					<td><a href="<?php echo admin_url('/edit-comments.php'); ?>"><?php echo bloginfo('name'); ?></a></td>
					<td><?php echo number_format($count); ?></td>
					<td style="text-align:center;"><a href="?action=delete_comments&amp;b_id=<?php echo $details['blog_id']; ?>" class="button secondary small">Delete Comments</a></td>
				</tr>
				<?php
				restore_current_blog();

			endforeach;
			?>
		</tbody>
		<tfoot>
			<tr>
				<th colspan="2" style="text-align:right;">Total:</th>
				<td style="text-align:left;"><?php echo number_format($total); ?></td>
				<td rowspan="3" style="text-align:center;"><a href="?action=delete_comments_all_blogs" class="button secondary small">Delete Comments<br /> From All Blogs</a></td>
			</tr>
			<tr>
				<th colspan="2" style="text-align:right;">Most:</th>
				<td style="text-align:left;"><?php echo number_format($most); ?></td>
			</tr>
			<tr>
				<th colspan="2" style="text-align:right;">Average:</th>
				<td style="text-align:left;"><?php echo number_format($total / count(hpu_get_blogs()) ); ?></td>
			</tr>
		</tfoot>
	</table>
</div>
<div class="grid-33 pr-0">
	<h2 class="mt-0">Results</h2>
	<?php if ( $results ): ?>

		<div class="<?php echo $error ? 'error' : 'notice'; ?>">

			<?php foreach( $results as $result ): ?>
				<?php if ( is_array($result) ): ?>
					<?php echo sprintf('<%1$s>%2$s</%1$s>', $result[1], $result[0]) ?>
				<?php else: ?>
					<p><?php echo $result; ?></p>
				<?php endif; ?>
			<?php endforeach; ?>
		</div>

	<?php else : ?>

		<p>No results to report.</p>

	<?php endif; ?>
</div>

<?php include_once 'footer.php'; ?>
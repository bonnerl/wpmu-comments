<?php 
/**
 * Display unauthorized page.
 * 
 * @author Lucas Bonner
 * @package WP Comments
 * @since WP Comments 0.1
 */
header('HTTP/1.0 401 Unauthorized'); 

include 'header.php';
?>

<h2>Unauthorized</h2>

<p>You do not have permissions to access this page.</p>

<p><a href="<?php echo wp_login_url( wpc_config::$url ); ?>" class="button">Login</a></p>

<?php include_once 'footer.php'; ?>
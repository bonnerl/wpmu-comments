<?php 
/**
 * Misc functions
 * 
 * @author Lucas Bonner
 * @package WP Comments
 * @since WP Comments 0.1
 */

/**
 * Get all of the blogs.
 * 
 * @since WP Comments 0.1
 */
function hpu_get_blogs()
{
	// Make sure $wpdb is accessible.
	global $wpdb;

	static $blogs;

	if ( empty($blogs) )
		$blogs = $wpdb->get_results( "SELECT * FROM {$wpdb->blogs} ORDER BY registered ASC", ARRAY_A );

	return $blogs;
}


/**
 * Delete all comments from blog.
 * 
 * @since WP Comments 0.1
 */
function hpu_comment_count(  )
{
	global $wpdb;

	$comments = $wpdb->get_row("SELECT count(comment_ID) as count FROM {$wpdb->comments};");

	return $comments->count;
}

/**
 * Disable comments on all posts.
 * 
 * @since WP Comments 0.1
 */
function hpu_disable_comments( $blog_id, &$results = array() )
{
	global $wpdb;

	// Set comment status to closed on all posts (regardless of post type)
	$result = $wpdb->update( $wpdb->posts, array('comment_status' => 'closed'), array('comment_status' => 'open'));

	if ( false === $results ) {
		$results[] = 'Could not display comments on posts.';

		return false;
	}

	$results[] = 'Disabled comments on '. $result .' posts.';

	return true;
}


/**
 * Disable pingbacks on all posts.
 * 
 * @since WP Comments 0.1
 */
function hpu_disable_pingbacks( $blog_id, &$results = array() )
{
	global $wpdb;

	// Set ping status to closed on all posts (regardless of post type)
	$result = $wpdb->update( $wpdb->posts, array('ping_status' => 'closed'), array('ping_status' => 'open'));

	if ( false === $results ) {
		$results[] = 'Could not display pingbacks on posts.';

		return false;
	}

	$results[] = 'Disabled pingbacks on '. $result .' posts.';

	return true;
}


/**
 * Truncate comments and comment meta  table.
 * 
 * @since WP Comments 0.1
 */
function hpu_truncate_comments( $blog_id, &$results = array() )
{
	global $wpdb;

	$count = hpu_comment_count();

	// Remove all comments
	$result = $wpdb->query("TRUNCATE {$wpdb->comments}");

	if ( false == $results ) {
		$results[] = 'Could not truncate Comments table.';

		return false;
	}

	$results[] = 'Comments table truncated. ('. $count .' comments)';

	// Remove all comments
	$result = $wpdb->query("TRUNCATE {$wpdb->commentmeta}");

	if ( false == $results ) {
		$results[] = 'Could not truncate Comment Meta table.';

		return false;
	}

	$results[] = 'Comment Meta table truncated.';

	return true;
}

/**
 * Delete all comments from blog.
 * 
 * @since WP Comments 0.1
 */
function hpu_reset_comment_count( $blog_id, &$results = array() )
{
	global $wpdb;

	// Set posts comment count to 0 on all posts (regardless of post type)
	$result = $wpdb->query("UPDATE {$wpdb->posts} SET comment_count = '0' WHERE comment_count != '0'");

	if ( false === $results ) {
		$results[] = 'Could not reset comment count on posts.';

		return false;
	}

	$results[] = 'Reset comment count on '. $result .' posts.';

	return true;
}

/**
 * Delete all comments from blog.
 * 
 * @since WP Comments 0.1
 */
function hpu_delete_all_comments( $blog_id, &$results = array() )
{
	$results[] = 'Deleting all comments&hellip;';

	if ( false == hpu_disable_comments($blog_id, $results) )
		return false;

	if ( false == hpu_disable_pingbacks($blog_id, $results) )
		return false;

	if ( false == hpu_truncate_comments($blog_id, $results) )
		return false;

	if ( false == hpu_reset_comment_count($blog_id, $results) )
		return false;

	$results[] = 'All comment clean up functions have been run successfully.';

	return true;
}
